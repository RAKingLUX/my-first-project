from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional

app = FastAPI() # -------------------------------------> App Name

class BlogPost(BaseModel):
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = 0
    highlight: Optional [str]
    
@app.post("/posts")
def posts(new_post: BlogPost):
    print(new_post)
    print(new_post.title)
    print(new_post.content)
    print(new_post.published)
    print(new_post.rating)
    
    return {"message": f"New blog posts added with title : {new_post.title}"}